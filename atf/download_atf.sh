#!/bin/sh

set -e

TFA_GIT_URL_DEFAULT=https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git
TFA_GIT_BRANCH_DEFAULT=master

if [ x"$TFA_GIT_URL" = x ]; then
    TFA_GIT_URL="$TFA_GIT_URL_DEFAULT"
fi

if [ x"$TFA_GIT_BRANCH" = x ]; then
    TFA_GIT_BRANCH="$TFA_GIT_BRANCH_DEFAULT"
fi

git clone --branch="${TFA_GIT_BRANCH}" "${TFA_GIT_URL}" trusted-firmware-a

