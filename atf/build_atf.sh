#!/bin/sh

set -e

make CROSS_COMPILE=aarch64-linux-gnu- realclean
make CROSS_COMPILE=aarch64-linux-gnu- PLAT=rk3399 LOG_LEVEL=50
