#!/bin/sh

apt-get update

# Install necessary packages
apt-get install -y debconf-utils
apt-get install -y ca-certificates
apt-get install -y wget
apt-get install -y git
apt-get install -y python3
apt-get install -y p7zip-full unzip
apt-get install -y ebtables
apt-get install -y lzop

# Adjust user groups
adduser vsoc-01 kvm
adduser vsoc-01 render
adduser vsoc-01 video

# Extra tools
cd /root
git clone https://github.com/matthuisman/gdrivedl.git

# Install android cuttlefish packages
wget -nv -c 'https://artifacts.codelinaro.org/artifactory/linaro-372-googlelt-rockpi4-cuttlefish-installer/rockpi4-cuttlefish-installer/latest/cuttlefish_packages.7z'

for i in *.7z; do
  7z x -aoa -- "${i}"
done

apt -o Apt::Get::Assume-Yes=true -o APT::Color=0 -o DPkgPM::Progress-Fancy=0 \
    install ./*.deb

adduser vsoc-01 cvdnetwork

# Install network manager
apt-get install -y network-manager

