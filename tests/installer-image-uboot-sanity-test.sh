#!/bin/bash

## check parameter
if [ x"$1" = x ]; then
    echo "Usage: $0 <image>"
    exit 2
fi

image="$1"

## check if image exist.
if [[ ! -e "${image}" ]]; then
    echo "Check: Error: image is not generated. Failed."
    exit 1
else
    echo "Check image exist: pass"
fi

## check image size. Should be at least 16MB.
imagesize=$(stat -c %s "${image}")
if [[ ${imagesize} -lt 16777216 ]]; then
    echo "Check: Error: image size too small. Failed."
    exit 1
else
    echo "Check image size: pass"
fi

SANITYCHECKTMPDIR=$(mktemp -d)

## check if TPL is in the image. idbloader is put on sector 64. However
## the header is encrypted. By offset 0x800 there is TPL, we can verify it.
## TPL will start with "RK33" as magic number.
dd if="${image}" of="${SANITYCHECKTMPDIR}/tplrk33" \
   bs=1 skip=34816 count=4
echo -n "RK33" > "${SANITYCHECKTMPDIR}/rk33"
if ! cmp -s "${SANITYCHECKTMPDIR}/tplrk33" "${SANITYCHECKTMPDIR}/rk33"; then
    echo "Check: Error: didn't found TPL"
    exit 1
else
    echo "Check TPL exist: pass"
fi

## check if U-boot.itb is in place located at sector 0x4000. Since it is
## an itb file the magic number is "d00dfeed".
dd if="${image}" of="${SANITYCHECKTMPDIR}/uboothead" \
   bs=1 skip=8388608 count=4
printf '\xd0\x0d\xfe\xed' > "${SANITYCHECKTMPDIR}/d00dfeed"
if ! cmp -s "${SANITYCHECKTMPDIR}/uboothead" "${SANITYCHECKTMPDIR}/d00dfeed"; then
    echo "Check: Error: didn't found U-boot"
    exit 1
else
    echo "Check U-boot exist: pass"
fi

rm -rf "${SANITYCHECKTMPDIR}"
