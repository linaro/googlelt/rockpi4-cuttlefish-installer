#!/bin/sh

BASEDIR=$(dirname $(realpath "$0"))

orig_iso=mini.iso
auto_extract_efi=1
efi_img=efi.img

new_files=iso_unpacked_and_modified
new_img=preseed-mini.img

PRESEEDFILE=$(realpath "${BASEDIR}"/preseed/preseed.cfg)
AFTERINSTALLSCRIPT=$(realpath "${BASEDIR}"/preseed/after_install_1.sh)

part_img_ready=1
if test "$auto_extract_efi" = 1; then
  start_block=$(/sbin/fdisk -l "$orig_iso" | fgrep "$orig_iso"2 | \
                awk '{print $2}')
  block_count=$(/sbin/fdisk -l "$orig_iso" | fgrep "$orig_iso"2 | \
                awk '{print $4}')
  if test "$start_block" -gt 0 -a "$block_count" -gt 0 2>/dev/null
  then
    dd if="$orig_iso" bs=512 skip="$start_block" count="$block_count" \
       of="$efi_img"
  else
    echo "Cannot read plausible start block and block count from fdisk" >&2
    part_img_ready=0
  fi
fi

# add preseed
mkdir ${new_files}
bsdtar -C ${new_files} -xf "$orig_iso"
cd ${new_files}
cp -f "${PRESEEDFILE}" preseed.cfg
cp -f "${AFTERINSTALLSCRIPT}" after_install_1.sh
chmod a+rx after_install_1.sh

# add preseed to console based installer
chmod ug+w initrd.gz
gzip -d -f initrd.gz
echo preseed.cfg | cpio -H newc -o -A -F initrd
echo after_install_1.sh | cpio -H newc -o -A -F initrd
gzip -9 initrd
chmod a-w initrd.gz
# add preseed to GTK based installer
chmod ug+w gtk
cd gtk
chmod ug+w initrd.gz
gzip -d -f initrd.gz
cp -f ../preseed.cfg .
cp -f ../after_install_1.sh .
echo preseed.cfg | cpio -H newc -o -A -F initrd
echo after_install_1.sh | cpio -H newc -o -A -F initrd
gzip -9 initrd
chmod a-w initrd.gz
rm -f preseed.cfg after_install_1.sh
cd ..
chmod a-w gtk
# modify Graphical installer to use tty1
chmod ug+w boot
chmod ug+w boot/grub
chmod ug+w boot/grub/grub.cfg
sed -i '0,/insmod gzio/{s#insmod gzio#set timeout=120\n\ninsmod gzio#}' boot/grub/grub.cfg
chmod a-w boot/grub/grub.cfg
chmod a-w boot/grub
chmod a-w boot
cd ..

rm -f "${new_img}"

# Create the new image if not partition extraction failed
test "$part_img_ready" = 1

echo "Create image ${new_img}..."

touch "${new_img}"
truncate -s 256M "${new_img}"

echo "Partition the image..."

/sbin/sgdisk \
 "-n:1:18M:+32M" "-t:1:ef00" "-c:1:debdiesp" \
 "-A:1:set:0" "${new_img}"

/sbin/sgdisk \
 "-n:2:50M:0" "-t:2:8305" "-c:2:debdilnr" \
 "-A:2:set:2" "${new_img}"

echo "Create EFI partition..."

system_partition=1
# Create an empty EFI system partition; it will be initialized later
system_partition_start=$(partx -g -o START -s -n "${system_partition}" "${new_img}" | xargs)
system_partition_end=$(partx -g -o END -s -n "${system_partition}" "${new_img}" | xargs)
system_partition_num_sectors=$((${system_partition_end} - ${system_partition_start} + 1))
system_partition_num_vfat_blocks=$((${system_partition_num_sectors} / 2))
#/sbin/mkfs.vfat -n SYSTEM -F 16 --offset=${system_partition_start} "${new_img}" ${system_partition_num_vfat_blocks}
system_temp_img="fat_895bdaac.img"
efi_img_temp_dir="efi1_776d9343"

dd if=/dev/zero of="${system_temp_img}" bs=512 count=${system_partition_num_sectors}
/sbin/mkfs.vfat -n SYSTEM -F 16 "${system_temp_img}" ${system_partition_num_vfat_blocks}
mkdir -p "${efi_img_temp_dir}"
fatcat efi.img -x "${efi_img_temp_dir}"
cd "${efi_img_temp_dir}"
mcopy -i ../"${system_temp_img}" -s * ::
cd -
rm -rf "${efi_img_temp_dir}"
/sbin/fsck.vfat "${system_temp_img}"
dd if="${system_temp_img}" of="${new_img}" bs=512 seek="${system_partition_start}" conv=fsync,notrunc
rm -f "${system_temp_img}"

echo "Creating rootfs partition..."

rootfs_partition=2
rootfs_partition_start=$(partx -g -o START -s -n "${rootfs_partition}" "${new_img}" | xargs)
rootfs_partition_end=$(partx -g -o END -s -n "${rootfs_partition}" "${new_img}" | xargs)
rootfs_partition_num_sectors=$((${rootfs_partition_end} - ${rootfs_partition_start} + 1))
rootfs_partition_offset=$((${rootfs_partition_start} * 512))
rootfs_partition_size=$((${rootfs_partition_num_sectors} * 512))

rootfs_temp_img="rootfs1_10c23155.img"

touch "${rootfs_temp_img}"
truncate -s "${rootfs_partition_size}" "${rootfs_temp_img}"
/sbin/mkfs.ext4 "${rootfs_temp_img}"

cd iso_unpacked_and_modified
find . -type d -exec e2mkdir ../"${rootfs_temp_img}":{} \;
find . -type f -exec e2cp -p {} ../"${rootfs_temp_img}":{} \;
cd -

dd if="${rootfs_temp_img}" of="${new_img}" bs=512 seek="${rootfs_partition_start}" conv=fsync,notrunc

rm -f "${rootfs_temp_img}"

/sbin/e2fsck -p -f "${new_img}"?offset=${rootfs_partition_offset} || true

echo "Flashing U-boot..."

dd if=idbloader.img of="${new_img}" seek=64 bs=512 conv=fsync,notrunc
#dd if=trust.img of="${new_img}" seek=24576 bs=512 conv=fsync,notrunc
dd if=u-boot.itb of="${new_img}" seek=16384 bs=512 conv=fsync,notrunc


#xorriso -as mkisofs \
#   -r -V 'Debian arm64 n' \
#   -o "$new_iso" \
#   -J -joliet-long -cache-inodes \
#   -e boot/grub/efi.img \
#   -no-emul-boot \
#   -append_partition 2 0xef "$efi_img" \
#   -partition_cyl_align all \
#   "$new_files"

# clean
rm -f efi.img
chmod ug+w -R "${new_files}"
rm -rf "${new_files}"
