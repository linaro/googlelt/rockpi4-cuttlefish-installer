#!/bin/sh

set +e

SELFPID=$$
renice 10 -p "$SELFPID"
ionice -c 3 -p "$SELFPID"

set -e

B=../uboot_build_place
mkdir -p "$B"

B=$(realpath "${B}")

export BL31=../trusted-firmware-a/build/rk3399/release/bl31/bl31.elf
cp -f "$BL31" "$B"
export BL31="$B"/bl31.elf

#export PMUM0=../rk3399-cortex-m0/rk3399m0.bin
#cp -f "$PMUM0" "$B"
#export PMUM0="$B"/rk3399m0.bin

export CROSS_COMPILE=aarch64-linux-gnu-

make O="$B" rock-pi-4-rk3399_defconfig
cat <<EOF > "${B}"/extraconfig
CONFIG_PROT_TCP=y
CONFIG_PROT_TCP_SACK=y
CONFIG_CMD_WGET=y
CONFIG_TCP_FUNCTION_FASTBOOT=y
CONFIG_UDP_FUNCTION_FASTBOOT=y
CONFIG_USB_FUNCTION_FASTBOOT=y
CONFIG_FASTBOOT_BUF_ADDR=0x3000000
CONFIG_FASTBOOT_BUF_SIZE=0x5000000
CONFIG_FASTBOOT_FLASH=y
CONFIG_FASTBOOT_FLASH_MMC_DEV=0
CONFIG_DM_GPIO=y
CONFIG_CMD_GPIO=y
CONFIG_ROCKCHIP_EFUSE=y
CONFIG_FS_EXT4=y
CONFIG_NET_RANDOM_ETHADDR=y
CONFIG_CMD_BCB=y
CONFIG_HUSH_PARSER=y
EOF
./scripts/kconfig/merge_config.sh -O ${B} ${B}/.config ${B}/extraconfig

make O="$B"
